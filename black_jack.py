# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 19:14:16 2019

@author: bruno
"""



from packages.entities.deck import Deck
from packages.entities.player import Player
from packages.entities.dealer import Dealer
from packages.entities.hand import Hand
from packages.entities.pot import Pot
from packages.entities.victory import Victory

from packages.game.state_of_game import State_Of_Game
from packages.game.quit import Quit
from packages.game.game_over import Game_Over

from packages.user_interface.menu import Menu
from packages.user_interface.hud import HUD



#%% 1- DECLARANDO FUNÇÕES:

# 1.1) Clear Screen: cls
def clearScreen(number):
    print('\n'*number)



#%% 1- GAME
class Game():
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor:
    def __init__(self):
        '''
        
        ... write something about this constructor ...
        
        '''
        
        # initialize deck
        self.deck = Deck()
        
        # initialize player and dealer
        self.player = Player()
        self.dealer = Dealer()        
        self.list_of_entities = [self.player, self.dealer]
        self.hand = Hand()
        self.pot = Pot()
        self.victory = Victory()
        
        # initialize state of game and initial menu
        self.state = State_Of_Game()
        self.menu = Menu()
        self.hud = HUD()
        
        
        self._is_running = True
        self.is_game_on = False
        
    
    
    # Method 1: tick
    def tick(self):
        '''        
        This method update the game moves.        
        '''
        
        # 1- menu
        if self.state.is_in_MENU():            
            self.menu.tick(self)
        
        # 2- pot
        elif self.state.is_in_POT():
            self.pot.tick(self)
        
        # 3- hand
        elif self.state.is_in_HAND():
            self.hand.tick(self)
        
        # 4- player turn
        elif self.state.is_in_PLAYER_TURN():
            self.player.tick(self)
        
        # 5- dealer turn
        elif self.state.is_in_DEALER_TURN():
            self.dealer.tick(self)
        
        # 6- victory
        elif self.state.is_in_CHECK_VICTORY():           
            self.victory.tick(self)
            
        # 7- check for a new deck
        if self.deck.get_number_of_remaining_cards() < 20:
            self.deck = Deck()
    
    
    # Method 2: render
    def render(self):
        '''        
        This method render in the screen the game's update.        
        '''
        
        clearScreen(100)
        
        self.hud.render(self) 
        
        if self.state.is_in_MENU():            
            self.menu.render(self)
            
        elif self.state.is_in_POT():
            self.pot.render(self)
            
        elif self.state.is_in_HAND():
            self.hand.render(self)
            
        elif self.state.is_in_PLAYER_TURN():
            self.player.render(self)
            
        elif self.state.is_in_DEALER_TURN():
            self.dealer.render(self)
            
        elif self.state.is_in_CHECK_VICTORY():
            self.victory.render(self)
        
    
    # Method 3 (private): 
    def check_keep_playing(self):
        '''
        
        Ask the player if he wants to keep playing.
        
        '''
        
        # 1- check for game over first
        if self.player.get_chips() <= 0:
            raise Game_Over("Sorry, you don't have any more chips. Better luck next time!")
        
        # 2- ask about play again
        while True:
            
            answer = input("Do you want to keep playing? 'yes [y]' or 'no [n]'? ")
        
            if answer.lower() == 'n':
                print('')
                raise Quit('You have quit the game. Comeback and play again later. Thanks!')
            
            if answer.lower() == 'y':
                clearScreen(100)
                
                # restart
                self.pot.is_pot = False
                self.hand.is_hand = False
                self.victory.is_victory = False
                
                # restart player and dealer
                for en in self.list_of_entities:
                    en.set_to_another_round()
                
                self.dealer.is_hidden = True              
                
                self.state.set_to_POT()
                
                break


    # Method x: run
    def run(self):
        
        while self._is_running:
            
            # update the game 
            self.tick()
            
            # render in the screen      
            self.render()
            


#%% 2- MAIN
if __name__ == "__main__":
    
    try:
        clearScreen(100)
        game = Game()
        game.run()
    except Quit as quit:
        print(quit)
    except Game_Over as game_over:
       print(game_over)       