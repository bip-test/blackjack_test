# -*- coding: utf-8 -*-
"""
Created on Sun Jun 16 15:48:26 2019

@author: bruno
"""



class Game_Over(Exception):
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor
    def __init__(self, msg):
        
        print('     ##### #####   #########   ##### #####     ')
        print('     ##### #####   GAME OVER   ##### #####     ')
        print('     ##### #####   #########   ##### #####     ')
        
        print('')
        
        super().__init__(msg)