# -*- coding: utf-8 -*-
"""
Created on Tue May 28 21:37:21 2019

@author: bruno
"""



class State_Of_Game():
    '''
    This class construct an a organized STATE OF GAME choices.
    
    '''
    
    # Constructor
    def __init__(self):
        '''
        
        This constructor starts this class in MENU choice. 
        
        '''
        self._current_state = ''
        self.set_to_MENU()
    
    
    # 1- about MENU
    def set_to_MENU(self):
        '''
        This Method set current state to MENU state.        
        '''
        self._current_state = 'MENU'
    
    
    def is_in_MENU(self):
        '''
        This Method check if the current state is in MENU.        
        '''        
        return self._current_state == 'MENU'
    
    
    # 2- about PLAYER TURN
    def set_to_PLAYER_TURN(self):
        '''
        This Method set current state to PLAYER TURN state.        
        '''
        self._current_state = 'PLAYER_TURN'
    
    
    def is_in_PLAYER_TURN(self):
        '''
        This Method check if the current state is in PLAYER TURN.        
        '''
        return self._current_state == 'PLAYER_TURN'
    
    
    # 3- about DEALER TURN
    def set_to_DEALER_TURN(self):
        '''
        This Method set current state to DEALER TURN state.        
        '''
        self._current_state = 'DEALER_TURN'
    
    
    def is_in_DEALER_TURN(self):
        '''
        This Method check if the current state is in DEALER TURN.        
        '''
        return self._current_state == 'DEALER_TURN'
    
    
    # 4- about POT
    def set_to_POT(self):
        '''
        This Method set current state to POT state.        
        '''
        self._current_state = 'POT'
    
    
    def is_in_POT(self):
        '''
        This Method check if the current state is in POT.        
        '''
        return self._current_state == 'POT'
    
    
    # 5- about HAND
    def set_to_HAND(self):
        '''
        This Method set current state to HAND state.        
        '''
        self._current_state = 'HAND'
    
    
    def is_in_HAND(self):
        '''
        This Method check if the current state is in HAND.        
        '''
        return self._current_state == 'HAND'
    
    
    # 6- about CHECK VICTORY
    def set_to_CHECK_VICTORY(self):
        '''
        This Method set current state to CHECK VICTORY state.        
        '''
        self._current_state = 'CHECK_VICTORY'
    
    
    def is_in_CHECK_VICTORY(self):
        '''
        This Method check if the current state is in CHECK VICTORY.        
        '''
        return self._current_state == 'CHECK_VICTORY'

