# -*- coding: utf-8 -*-
"""
Created on Tue May 28 21:35:13 2019

@author: bruno
"""



from random import shuffle

from packages.entities.card import Card



class Deck():
    '''
    Esta classe monta um baralho embaralhado fazendo uso da clase Card. Os atributos e métodos são:
    
    Atributos:
        -> ._cards é um atributo oculto, que corresponde a uma lista de todas as cartas do baralho.
    
    Métodos:
        -> ._set_deck é um método oculto que monta a lista de cartas embaralhadas.
        
        -> get_card é um método que retira uma carta do baralho, retornando esta carta. Ao mesmo
        tempo que esta carta é retornada, ela é excluída da lista de cartas, ficando com menos uma.
    '''
    
    # Constructor
    def __init__(self):
        '''
        
        ... write something about this constructor ...
        
        '''
        self._cards = self._set_deck() # list of Card
     
          
    # Method 1: _set_deck
    def _set_deck(self):
        '''
        This methods creates a list of shuffler cards of one deck.
        '''
        ranks = [str(n) for n in range(2,11)] + list('JQKA') # list of possibles ranks
        suits = 'Spades Diamonds Clubs Hearts'.split() # list of possibles suits
        
        cards = [Card(rank, suit) for suit in suits
                                  for rank in ranks]
        
        shuffle(cards)
        
        return cards
    
    
    # Method 2: hit
    def get_card(self):
        '''
        
        ... write something about this method ...
        
        '''
        # choosing a card from the deck
        return self._cards.pop(0)
    
    
    # Method 3: get number of remaining cards
        '''
        
        ... write something about this method ...
        
        '''
    def get_number_of_remaining_cards(self):
        return len(self._cards)
    
   
    