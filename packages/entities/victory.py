# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 18:30:35 2019

@author: bruno
"""



class Victory():
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor:
    def __init__(self):
        '''
        
        ... write something about this constructor ...
        
        '''
        
        self.winner_name = ''
        self.is_victory = False        
        self.is_triggered = False
    
    
    # Method 1: tick
    def tick(self, game):
        
        if self.is_triggered:
            
            self.is_victory = True
            self.is_triggered = False            
            
            if game.player.is_bust:
                self.winner_name = game.dealer.__class__.__name__
            
            elif game.dealer.is_bust:
                self.winner_name = game.player.__class__.__name__
                
                game.player.set_chips( game.pot.get_pot() )                
            
            elif game.player.get_points() > game.dealer.get_points():
                self.winner_name = game.player.__class__.__name__
                
                game.player.set_chips( game.pot.get_pot() )
                
            else:
                self.winner_name = game.dealer.__class__.__name__
            
    
    
    # Method 2: render
    def render(self, game):
        self.is_triggered = True    
            