# -*- coding: utf-8 -*-
"""
Created on Tue May 28 21:40:25 2019

@author: bruno
"""



from packages.entities.entity import Entity

import time



class Dealer(Entity):
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor
    def __init__(self):
        '''
        
        ... write something about this constructor ...
        
        '''
        Entity.__init__(self)
        self._move = '' # see dealer or quit
        self.is_hidden = True       
        self.is_triggered = False
        
    
    # Method 1: tick
    def tick(self, game):
        '''
        
        ... write something about this method ...
        
        '''
        if self.is_triggered:
            
            self.is_triggered = False
            self.is_hidden = False
            
            self.set_points()
        
            # ckeck if player is bust
            if game.player.is_bust:
                game.state.set_to_CHECK_VICTORY()
            
            # ckeck if dealer beated the player
            elif self._points >= game.player.get_points():
                game.state.set_to_CHECK_VICTORY()
            
            # ckeck if dealer's points is over 21: bust
            elif self.is_bust:
                game.state.set_to_CHECK_VICTORY()            
        
            else:
                
                # get card from deck
                new_card = game.deck.get_card()
                
                # storage the card in the cards list
                self._cards.append(new_card)
                
                # update dealer points
                self.set_points()
                
    
    # Method 2: get entity points
    def get_points(self):
        '''
        
        ... write something about this method ...
        
        '''
        cards = self.get_cards()
        
        if self.is_hidden:            
            
            if cards[1].rank == 'A':
                return 11  
            
            if cards[1].rank in list('JQK'):
                return 10
            
            if cards[1].rank in [str(n) for n in range(2,11)]:
                return int(cards[1].rank)
        else:
            return self._points
    
    
    # Method 3: render
    def render(self, game):
        
        print("### DEALER'S TURN:")
        print('')
        print(('  -> *** WAIT *** '))
        
        time.sleep(1.5)
        self.is_triggered = True
                
            
        