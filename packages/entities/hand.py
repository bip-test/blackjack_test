# -*- coding: utf-8 -*-
"""
Created on Sun Jun 16 20:45:16 2019

@author: bruno
"""



class Hand():
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor
    def __init__(self):        
        self.is_hand = False
        self.is_triggered = False
    
    
    # Method 1: tick
    def tick(self, game):
        
        if self.is_triggered:
        
            for en in game.list_of_entities:
                
                for _ in range(0,2):
                    
                    # get card from deck
                    new_card = game.deck.get_card()
                    
                    # storage the card in the cards list
                    en.set_cards(new_card)
                    
                    # update en (entitir) points
                    en.set_points()
                                    
            self.is_hand = True
            self.is_triggered = False
            
            game.state.set_to_PLAYER_TURN()
            
            
    
    
    # Method 2: render
    def render(self, game):        
        self.is_triggered = True
            
 