# -*- coding: utf-8 -*-
"""
Created on Tue May 28 21:40:25 2019

@author: bruno
"""



from packages.entities.entity import Entity

from packages.game.quit import Quit



class Player(Entity):
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor
    def __init__(self, chips=1e3):
        '''
        
        ... write something about this constructor ...
        
        '''
        Entity.__init__(self)        
        self._chips = int(chips)
        self._move = '' # hit, stay or quit
        self.is_hidden = False
        self.is_triggered = False
    
    
    # Method 1: set player chips
    def set_chips(self, amount):
        '''
        
        ... write something about this method ...
        
        '''
        self._chips += amount
        
        
    # Method 2: get player chips
    def get_chips(self):
        '''
        
        ... write something about this method ...
        
        '''
        return self._chips
    
    
    #  Method 3: tick (override)
    def tick(self, game):
        '''
        
        ... write something about this method ...
        
        '''
        if self.is_triggered:
            
            self.is_triggered = False
        
            # if hit
            if self._move.lower() == 'h':
                
                # get card from deck
                new_card = game.deck.get_card()
                
                # storage the card in the cards list
                self._cards.append(new_card)
                
                # update players points
                self.set_points()
                
                if self.is_bust:
                    game.state.set_to_DEALER_TURN()
                
            elif self._move.lower() == 's':
                game.state.set_to_DEALER_TURN()
        
        
    # Method 4: render
    def render(self, game):
        
        print("### PLAYER'S TURN:")
        
        # ask the player if he still playing bye choosing "hit" or "stay" 
        while True:           
            self._move = input("  -> hit [h], stand [s] or quit[q]? ")
            
            if self._move.lower() == 'h' or self._move.lower() == 's' or self._move.lower() == 'q':
                self.is_triggered = True
                break
        
        if self._move.lower() == 'q':
            raise Quit('  -> You have quit the game!')
        
        