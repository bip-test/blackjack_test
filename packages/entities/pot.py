# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 15:17:34 2019

@author: bruno
"""



class Pot():
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor:
    def __init__(self):
        '''
        
        ... write something about this constructor ...
        
        '''
        self.is_pot = False
        self.is_triggered = False
        
        self._bet_amount = 0
        self._amount = 0
    
    
    # Method 1: tick
    def tick(self, game):
        '''
        
        ... write something about this method ...
        
        '''
        if self.is_triggered:
            
            game.player.set_chips(-self._bet_amount)
            
            self._amount = 2 * self._bet_amount
            
            self.is_triggered = False            
            self.is_pot = True
            
            game.state.set_to_HAND()
    
    
    # Method 2: render
    def render(self, game):
        '''
        
        ... write something about this method ...
        
        '''
        print('### SETTING BET POT:')
        print('')
        
        # get player bet
        print('  -> You have ' + str(game.player.get_chips()) + ' chips.')
        
        self._bet_amount = self._ask_for_positive_int(game, '  -> Please, place your bet: ')
        
        self.is_triggered = True
    
    
    # Method 3: get bet
    def get_pot(self):
        '''
        
        ... write something about this method ...
        
        '''        
        return self._amount
    
    
    # Method 5 (private): ask for int
    def _ask_for_positive_int(self, game, msg):
        
        while True:
            
            try:
                answer = int( input(msg) )
            except:
                print('  -> Sorry, this is not a number. Please choose a positive integer.')
            else:
                if answer < 0:
                    print('  -> Sorry, please choose a positive integer.')
                elif answer > game.player.get_chips():
                    print("  -> Sorry, your bet can't exceed", game.player.get_chips())
                else:
                    break
                    
        return answer
    
    
    