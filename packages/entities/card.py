# -*- coding: utf-8 -*-
"""
Created on Tue May 28 21:17:51 2019

@author: bruno
"""



class Card():
    '''
    Esta classe define uma carta com 3 atributos: rank (A,K,Q,J ou números) e suit (naipe).
    
    -> .rank é do tipo string (str)
    
    -> .suit é do tipo string (str)
    '''
    
    # Constructor
    def __init__(self, rank, suit):
        '''
        
        ... write something about this constructor ...
        
        '''        
        self._set_rank_suit_value(rank, suit)
    
    
    # set rank, suit and value of a card
    def _set_rank_suit_value(self, rank, suit):
        '''
        
        ... write something about this method ...
        
        '''
        self.rank = str(rank)
        self.suit = str(suit)
    
    # __repr__
    def __str__(self):
        '''
        
        ... write something about this method ...
        
        '''
        return '%s of %s' % (self.rank, self.suit)
        
#        return '%s: %s of %s' % (self.__class__.__name__,
#                                 self.rank,
#                                 self.suit)

