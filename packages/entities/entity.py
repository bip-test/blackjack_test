# -*- coding: utf-8 -*-
"""
Created on Tue May 28 21:38:27 2019

@author: bruno
"""



class Entity():
    '''
    
    ... write something about this class ...
    
    '''
    
    # Constructor
    def __init__(self):
        '''
        
        ... write something about this constructor ...
        
        '''
        self._cards = []
        self._points = 0
        self.is_bust = False
        self.is_hidden = False
        self.is_triggered = False
    
    
    # Method 1: set entity points
    def set_points(self):
        '''
        
        ... write something about this method ...
        
        '''
        
        value = 0
        is_A_in_cards = False
        
        # sum all cards values considering card A with value = 1
        for card in self._cards:
            
            if card.rank in [str(n) for n in range(2,11)]:
                value += int(card.rank)
            
            if card.rank in list('JQK'):
                value += 10
            
            if card.rank == 'A':
                value += 1
                is_A_in_cards = True
        
        # if exist card A, check with if it can be value = 11       
        if is_A_in_cards and value <= 11:
            value += 10 # already added 1 before
        
        # update entity value points
        self._points = value
        
        # check if bust
        if self._points > 21:
            self.is_bust = True
    
    
    # Method 2: get entity points
    def get_points(self):
        '''
        
        ... write something about this method ...
        
        '''
        return self._points
    
    
    # Method 3: set entity cards
    def set_cards(self, new_card):
        self._cards.append(new_card)
    
    
    # Method 4: get entity cards
    def get_cards(self):
        return self._cards
    
    
    # Method 5: set to another round
    def set_to_another_round(self):
        self._cards = []
        self._points = 0
        self.is_bust = False
        
    
    # Method 6: print card
    def print_card(self, card):
        print('     - ' + 'Card ' + str(card) + ': ', end='')
        print(self.get_points(), 'points.')
    
    
    # Method 7: tick
    def tick(self, game):
        pass
    
    
    # Method 8: render
    def render(self, game):
        pass
            
            
            
            
            
            
            
        
        
    
    
    

