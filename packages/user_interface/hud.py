# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 21:00:03 2019

@author: bruno
"""



class HUD():
    
    
    # Constructor:
    def __init__(self):
        pass
    
    
    # Method 1: tick
    
    
    # Method 2: render
    def render(self, game):
        
        if game.is_game_on:
            
            # game title
            print('###############################################')
            print('################   Black Jack  ################')
            print('###############################################')
            print('')
            print('')
            
        
        if game.pot.is_pot:
            
            # pot
            print("### POT:")
            print('')
            
            # print value of the pot
            print('   # Pot Value: ' + str(game.pot.get_pot()) )
            print('')
            print('')
        
        
        if game.hand.is_hand:
            
            # hand
            print("### HAND:")
            print('')
            
            for en in game.list_of_entities:
                
                name = str(en.__class__.__name__)
                
                # print card
                print('   # ' + name )                
                      
                for card in en.get_cards():
                    
                    index = en.get_cards().index(card) + 1
                    
                    if index-1 == 0 and name == 'Dealer' and en.is_hidden:
                        print('     - ' + 'Card ' + str(index) + ': hidden')
                    else:
                        print('     - ' + 'Card ' + str(index) + ': ' + str(en.get_cards()[index-1]))
                
                
                # print value so far:
                print('')
                if en.is_bust:
                    print('     - points: ' + str(en.get_points()) + ' (BUST)')
                else:
                    print('     - points: ' + str(en.get_points()))
                print('')
                print('')
                
        
        if game.victory.is_victory:
            
            # victory
            print("### WINNER:")
            print('')
            
            print('   # ' + game.victory.winner_name + ' wins!' )
            print('')
            print('')
            
            game.check_keep_playing()

