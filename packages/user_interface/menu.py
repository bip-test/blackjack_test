# -*- coding: utf-8 -*-
"""
Created on Sun Jun 16 00:27:42 2019

@author: bruno
"""



from packages.game.quit import Quit

import time



class Menu():
    
    
    # Constructor:
    def __init__(self):
        
        self._answer = ''
        self.is_triggered = False
    
   
    # Method 1:
    def tick(self, game):
        
        if game.state.is_in_MENU() and self.is_triggered:            
            
            if self._answer.lower() == 'y':                
                self._answer = ''
                self.is_triggered = False
                
                game.state.set_to_POT()
                game.is_game_on = True 
    
    
    # Method 2:
    def render(self, game):
        
        if game.state.is_in_MENU():
        
            print('###############################################')
            print('##########  Wellcome to Black Jack!  ##########')
            print('###############################################')
            print('')
            
            while True:
                self._answer = input("  -> Are ready to play? yes[y] or no[n]? ")
                
                if self._answer.lower() == 'y':
                    self.is_triggered = True
                    
                    print('')
                    print("  -> Let's Play! ! !")
                    time.sleep(2)
                    break
                elif self._answer.lower() == 'n':
                    raise Quit('  -> You have quit the game!')
        
        
        